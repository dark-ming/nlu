transformer原理:
- [详解Transformer （Attention Is All You Need）](https://zhuanlan.zhihu.com/p/48508221)
- [The Illustrated Transformer](http://jalammar.github.io/illustrated-transformer/)
- [图解Transformer（完整版）](https://blog.csdn.net/longxinchen_ml/article/details/86533005)

transformer代码解析：
- [The Annotated Transformer](http://nlp.seas.harvard.edu/2018/04/03/attention.html#batches-and-masking)
- [搞懂Transformer结构，看这篇PyTorch实现就够了（上）](https://zhuanlan.zhihu.com/p/48731949)
- [Transformer代码阅读](http://fancyerii.github.io/2019/03/09/transformer-codes/)